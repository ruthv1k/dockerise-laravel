# Dockerise Laravel

This is basic example of a dockerised laravel project

## Services inside docker
- nginx
- mysql
- php

## Instructions
- To start the containers right after building them

    - `docker-compose up -d --build`

- To fix the premissions issue for the storage folder

    - `docker-compose exec app chmod 777 -R storage`

- To generate a key

    - `docker-compose exec app php artisan key:generate`

- Visit localhost, the server runs on default port `80`

- To connect to db service, update the credentials inside `.env` file as per the docker-compose credentials

- To import any exisitng sql to into the db, export the sql file and copy its contents to `docker/mysql/db.sql` file

Note: The current docker setup is not optimised hence the build might take upto 10-20 min depending on your computer